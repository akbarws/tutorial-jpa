package com.akbar.springjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.akbar.springjpa.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> { }