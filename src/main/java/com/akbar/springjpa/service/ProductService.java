package com.akbar.springjpa.service;

import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.akbar.springjpa.domain.ProductRegister;
import com.akbar.springjpa.entity.Product;
import com.akbar.springjpa.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	ProductRepository productRepositoryImpl;
	
	public Long add(ProductRegister productRegister) {
		Product product = new Product();
		product.setName(productRegister.getName());
		product.setPrice(productRegister.getPrice());
		return productRepositoryImpl.save(product).getId();
	}
	
	public HashMap<String, Object> get() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		List<Product> products = productRepositoryImpl.findAll();
		
		result.put("count", products.size());
		result.put("product1", products.get(0));
		result.put("product2", products.get(1));
		
		HashMap<String, Object> response = new HashMap<String, Object>();
		response.put("code", 200);
		response.put("message", "Success");
		response.put("data", result);
		
		return response;
	}
	
	public String info() {
		String getUserEndpoint = "http://localhost:8200/get";
		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.getForObject(getUserEndpoint, String.class);
		JSONObject responseObject = new JSONObject(result);
		Object name = responseObject.getJSONObject("data").getJSONObject("product2").get("name");
		return name.toString();
	}
}
