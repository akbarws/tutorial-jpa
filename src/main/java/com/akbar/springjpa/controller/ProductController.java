package com.akbar.springjpa.controller;

import java.net.URI;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.akbar.springjpa.domain.ProductRegister;
import com.akbar.springjpa.service.ProductService;

@RestController
public class ProductController {

	@Autowired
	ProductService productServiceImpl;
	
	@PostMapping(value = "/add", consumes = "application/json", produces = "application/json")
	public HashMap<String, String> add(@RequestBody ProductRegister productRegister) {
		HashMap<String, String> result = new HashMap<String, String>();
		result.put("id", productServiceImpl.add(productRegister).toString());
		return result;
	}
	
	@GetMapping(value = "/get", produces = "application/json")
	public HashMap<String, Object> get() {
		return productServiceImpl.get();
	}
	
	@GetMapping(value = "/info", produces = "application/json")
	public String info() {
		return productServiceImpl.info();
	}
	
	@RequestMapping(value = "/redirect")
	public ResponseEntity<Void> redirect(HttpServletRequest request) {
		return ResponseEntity.status(HttpStatus.FOUND)
		        .location(URI.create("http://www.google.co.id"))
		        .build();
	}
}
