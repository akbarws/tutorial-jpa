package com.akbar.springjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TutorialSpringJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TutorialSpringJpaApplication.class, args);
	}

}
